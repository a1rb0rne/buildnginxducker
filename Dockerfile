FROM debian:9 as build

RUN apt-get update \
&& apt-get -y install gcc \
&& apt-get -y install wget \
&& apt-get -y install make \
&& apt-get -y install git 

RUN wget https://nginx.org/download/nginx-1.19.8.tar.gz && tar xvfz nginx-1.19.8.tar.gz \
&& wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.19.tar.gz && tar xvfz v0.10.19.tar.gz  \
&& wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar xvfz v0.3.1.tar.gz \
&& wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20201027.tar.gz && tar xvfz v2.1-20201027.tar.gz \
&& cd luajit2-2.1-20201027 \
&& make install \
&& cd ../nginx-1.19.8 \
&& export LUAJIT_LIB=/usr/local/lib/ \
&& export LUAJIT_INC=/usr/local/include/luajit-2.1/ \
&& ./configure --add-dynamic-module=/lua-nginx-module-0.10.19 --add-dynamic-module=/ngx_devel_kit-0.3.1 --without-http_rewrite_module --without-http_gzip_module \
&& make \
&& make install  

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]